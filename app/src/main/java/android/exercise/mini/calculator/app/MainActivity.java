package android.exercise.mini.calculator.app;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.Serializable;


public class MainActivity extends AppCompatActivity {

  @VisibleForTesting
  public SimpleCalculator calculator;








  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    if (calculator == null) {
      calculator = new SimpleCalculatorImpl();
    }

    /*
    TODO:
    - find all views
    - initial update main text-view based on calculator's output
    - set click listeners on all buttons to operate on the calculator and refresh main text-view
     */

    View button0 = findViewById(R.id.button0);
    View button1 = findViewById(R.id.button1);
    View button2 = findViewById(R.id.button2);
    View button3 = findViewById(R.id.button3);
    View button4 = findViewById(R.id.button4);
    View button5 =findViewById(R.id.button5);
    View button6 = findViewById(R.id.button6);
    View button7 = findViewById(R.id.button7);
    View button8 = findViewById(R.id.button8);
    View button9 = findViewById(R.id.button9);
    View buttonBackspace = findViewById(R.id.buttonBackSpace);
    View buttonClear = findViewById(R.id.buttonClear);
    View buttonPlus = findViewById(R.id.buttonPlus);
    View buttonMinus = findViewById(R.id.buttonMinus);
    View buttonEquals = findViewById(R.id.buttonEquals);
    TextView textViewOutput = findViewById(R.id.textViewCalculatorOutput);


    textViewOutput.setText(calculator.output());
    textViewOutput.setVisibility(View.VISIBLE);

    setClickListener(button0, 0);
    setClickListener(button1, 1);
    setClickListener(button2, 2);
    setClickListener(button3, 3);
    setClickListener(button4, 4);
    setClickListener(button5, 5);
    setClickListener(button6, 6);
    setClickListener(button7, 7);
    setClickListener(button8, 8);
    setClickListener(button9, 9);


    buttonBackspace.setOnClickListener(v -> {
      calculator.deleteLast();
      textViewOutput.setText(calculator.output());
      textViewOutput.setVisibility(View.VISIBLE);

    });

    buttonClear.setOnClickListener(v -> {
      calculator.clear();
      textViewOutput.setText(calculator.output());
      textViewOutput.setVisibility(View.VISIBLE);

    });

    buttonPlus.setOnClickListener(v -> {
      calculator.insertPlus();
      textViewOutput.setText(calculator.output());
      textViewOutput.setVisibility(View.VISIBLE);

    });

    buttonMinus.setOnClickListener(v -> {
      calculator.insertMinus();
      textViewOutput.setText(calculator.output());
      textViewOutput.setVisibility(View.VISIBLE);
    });

    buttonEquals.setOnClickListener(v -> {
      calculator.insertEquals();
      textViewOutput.setText(calculator.output());
      textViewOutput.setVisibility(View.VISIBLE);

    });






  }

  @Override
  protected void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putSerializable("saveState", calculator.saveState());
  }

  @Override
  protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    Serializable saveState = savedInstanceState.getSerializable("saveState");
    calculator.loadState(saveState);
    TextView textViewOutput = findViewById(R.id.textViewCalculatorOutput);
    textViewOutput.setText(calculator.output());
    textViewOutput.setVisibility(View.VISIBLE);

  }

  private void setClickListener(View view, int digit){
    view.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v){
        calculator.insertDigit(digit);
        TextView textViewOutput = findViewById(R.id.textViewCalculatorOutput);
        textViewOutput.setText(calculator.output());
        textViewOutput.setVisibility(View.VISIBLE);
      }
    });

  }



}