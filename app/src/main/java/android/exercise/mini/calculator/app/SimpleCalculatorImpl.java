package android.exercise.mini.calculator.app;

import android.view.View;

import android.widget.EditText;
import android.widget.TextView;


import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;

public class SimpleCalculatorImpl implements SimpleCalculator {
  String output = "";
  Boolean num_order = false;


  @Override
  public String output() {
    if (output.equals("")){
      output = "0";
    }
    return output;
  }

  @Override
  public void insertDigit(int digit) throws RuntimeException {
    num_order = false;
    if (0 <= digit && digit <= 9) {
      if (output.equals("0")){
        output = Integer.toString(digit);
      }
      else {
        output = output + (Integer.toString(digit));
      }
    }
    else {
      throw new RuntimeException();
    }
  }



  @Override
  public void insertPlus() {
    if (output.equals("")){
      output = "0";
    }
    if (!num_order){
      output = output + "+";
      num_order = true;

    }
  }

  @Override
  public void insertMinus() {
    if (output.equals("")){
      output = "0";
    }
    if (!num_order){
      output = output + "-";
      num_order = true;

    }
  }


  @Override
  public void insertEquals() {

    String num = "";
    int endnum = 0 ;
    int i = 0;
    while (i < output.length()){
      char c = output.charAt(i);
      if (c != '+' && c != '-'){
        num = num + c;
      }

      if (c == '-' && i != output.length() -1){
        endnum += Integer.parseInt(num);
        num = "-";


      }
      if (c == '+' && i != output.length() -1 ) {
        endnum += Integer.parseInt(num);
        num = "";

      }
      i++;
    }
    endnum += Integer.parseInt(num);
    output = Integer.toString(endnum);
  }

  @Override
  public void deleteLast() {
    if (!output.equals("")){
      output = output.substring(0, output.length() - 1);
    }
    num_order = !num_order;

    if (output.length() == 0){
      num_order = false;
    }
  }

  @Override
  public void clear() {

    output = "0";
    num_order = false;
  }

  @Override
  public Serializable saveState() {
    CalculatorState state = new CalculatorState();
    state.num_order = num_order;
    state.output = output;
    return state;
  }

  @Override
  public void loadState(Serializable prevState) {
    if (!(prevState instanceof CalculatorState)) {
      return; // ignore
    }
    CalculatorState casted = (CalculatorState) prevState;
    output = casted.output ;
    num_order = casted.num_order;
  }

  private static class CalculatorState implements Serializable {
    String output;
    Boolean num_order;
  }




}
